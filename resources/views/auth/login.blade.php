<x-layout>
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-md-6 col-12 order-2 order-md-1">
                <img src="/frontend/piantaccedi.png" alt="" class="img-fluid pianta-accedi">
            </div>

            <div class="d-none" id="titlePage">Login</div>
            
            <div class="col-11 col-md-4 p-5 my-md-5 mx-md-5 bg-mainlight shadow-box order-1 order-md-2 mt-3">
                <h1 class="my-3 text-center fs-2" id="">{{__('ui.access')}}!</h1>
            <form method="POST" action="{{route('login')}}" class="mt-5">
            @csrf

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">{{__('ui.mail')}}</label>
                <input type="email" class="form-control border border-dark rounded-0" name="email">
            </div>

            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">{{__('ui.password')}}</label>
                <input type="password" class="form-control border border-dark rounded-0" name="password">
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <button type="submit" class="btn mt-4 w-100 btn_giallo">{{__('ui.access')}}</button>

            <h4 class="text-center w-100 my-5">{{__('ui.no_account')}} <br>
                <a href="{{route('register')}}" class="">{{__('ui.no_account1')}} </a> {{__('ui.no_account2')}}
            </h4>
        </form>
            </div>
        </div>
    </div>
</x-layout>


<div class="col-6">

</div>
