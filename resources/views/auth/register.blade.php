<x-layout>
    <div class="container bg-plant">
        <div class="row justify-content-center">
        <div class="col-md-6 col-12 order-2 order-md-1">
                <img src="/frontend/piantaregistra.png" alt="" class="img-fluid pianta-accedi">
            </div>
            <div class="col-11 col-md-4 p-5 my-md-5 mx-md-5 bg-mainlight shadow-box order-1 order-md-2 mt-3">
            
                <div class="d-none" id="titlePage">Register</div>
            
                <h1 class="my-3 text-center fs-2">{{__('ui.no_account1')}}!</h1>
                <form method="POST" action="{{route('register')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">{{__('ui.username')}}</label>
                        <input type="text" class="form-control border border-dark rounded-0 " name="name">
                    </div>
            
                    <div class="mb-3">
                      <label for="exampleInputEmail1" class="form-label">{{__('ui.mail')}}</label>
                      <input type="email" class="form-control border border-dark rounded-0 " name="email">
                    </div>
            
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">{{__('ui.password')}}</label>
                      <input type="password" class="form-control border border-dark rounded-0 " name="password">
                    </div>
            
                    <div class="mb-3">
                        <label for="password_confirmation" class="form-label">{{__('ui.repeat')}} </label>
                        <input type="password" class="form-control border border-dark rounded-0 " name="password_confirmation">
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            
                    <button type="submit" class="btn w-100 btn_giallo">{{__('ui.no_account1')}}</button>
                    
                    
                  </form>
            </div>
        </div>
    </div>
</x-layout>