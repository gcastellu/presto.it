<div>
    <div class="input-group">
        <span class="input-group-text border-dark rounded-0 bg-maincolor fs-button fs-6"
        id="basic-addon1"><i class="bi bi-search text-white fs-5"></i>
        <div wire:loading>
            {{-- <div class="ms-2 spinner-grow spinner-grow-sm text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div> --}}
            <div class="ms-2 spinner-border spinner-border-sm text-light" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
    </span>
        <input type="text" wire:model='query' class="form-control border-dark rounded-0 bg-mainlight" placeholder='{{__('ui.placeholderSearch')}}'
        wire:keydown.escape ='resetQuery'
        wire:keydown.arrow-up ='decrementSelectIndex'
        wire:keydown.arrow-down = 'incrementSelectIndex'
        wire:keydown.enter="selectAds">

    </div>
    {{-- <input name="searched" class='form-control border-dark rounded-0 bg-mainlight' type="search" aria-label='Search' placeholder="Macchina, Vestiti..."> --}}
    

    
    
    
    
    
    @if(!empty($query))
    {{-- escape al click fuori del search --}}
    <div class="position-fixed top-0 start-0 bottom-0 end-0" wire:click='resetQuery'></div>
    {{-- fine escape al click fuori del search --}}
    
    <div class="position-absolute z-10 list-group p-0">
        {{-- <div class="row"> --}}
            {{-- {{dd($categories['name'])}} --}}
            {{-- <div class="col-4 px-3"> --}}
                @forelse ($announcements as $i => $announcement)
                {{-- @dd($announcement['id']) --}}
                {{-- @dd($images[7]->path) --}}
                <a href="{{ route('announcement.show', $announcement['id']) }}" 
                    class="list-group-item nav-link w-100 ms-5 me-5 pe-5 rounded-0 {{$selectIndex === $i ? 'bg-mainlight' : ''}} p-1 fw-bold p-0 m-0">
                    {{-- <img style="height: 50px; width: 50px; margin-right: 5px" class="img-fluid rounded-5" src="">  --}}
                    {{ $announcement['title'] }}
                    <span class="in_category"><i>in {{$categories[$announcement['category_id']-1]->name}}</i></span>
                </a>
                {{-- </div> --}}
                @empty
                <a  class="list-group-item nav-link bg-mainlight p-1 fw-bold p-0 m-0">
                    <i class="bi bi-exclamation-octagon-fill text-danger text-center fs-3 ms-md-2 p-0 list-group-item border border-0 bg-mainlight">
                        <span class="fs-6">Nessun annuncio trovato!</span>
                    </i>
                </a>
                {{-- <div class="row w-100">
                    <div class="col-2">
                        <i class="bi bi-exclamation-octagon-fill text-danger text-center fs-3 ms-md-2 p-0 list-group-item border border-0 bg-mainlight"></i>
                    </div>
                    <div class="col-10 p-0">
                        <div class="list-group-item border border-0 bg-mainlight fw-bold">{{__('ui.no_result')}}</div>
                    </div>
                </div> --}}
                @endforelse
            {{-- </div> --}}
    </div>
    @endif
</div>
