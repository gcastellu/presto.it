<x-layout>
    <div class="min-vh-100">
        <div class="container">
            <div class="row">
                <div class="col-12 my-4">
                    <h1>{{__('ui.explore')}} : {{ $category->name }}</h1>
                </div>
            </div>
        </div>
    
        <div class="container">
            <div class="row">
                @forelse ($category->announcements as $announcement)
                            <div class="col-12 col-md-4 my-0">
    
                                <div class="card border border-dark rounded-0">
                                    <img class="rounded-0" src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(660, 440): 'https://picsum.photos/400' }}"
                            class="card-img-top" alt="...">
                                    <div class="card-body bg-light p-0">
                                        <h3 class="px-2 py-1 card-title">{{ $announcement->title }}</h3>
                                        <h4 class="px-2 py-1 card-text">{{ $announcement->body }}</h4>
                                        <h4 class="px-2 py-1 card-text">{{ $announcement->price }}</h4>
    
                                        <a href="{{route('announcement.show', compact('announcement'))}}" 
                                            class="btn btn_verde ms-5 w-75 border border-dark rounded-0 bg-maincolor ">{{__('ui.view')}}</a>
    
                                        
    
                                        <p class="card-footer mt-2 mb-0">{{__('ui.publish')}} :
                                            {{ $announcement->created_at->format('d/m/Y') }} - {{__('ui.author')}} :
                                            {{ $announcement->user->name ?? '' }}
                                        </p>
    
                                    </div>
                                </div>
    
                            </div>
    
    
    
                        @empty
    
                            <div class="col-12 text-center">
                                <p class="h1">{{__('ui.no_ads')}}</p>
                                <p class="h2">{{__('ui.no_ads1')}}</p>
                                <a href="{{ route('announcements.create') }}" class="fs-5 mt-2 btn btn_verde">{{__('ui.no_ads2')}}</a>
                            </div>
                        @endforelse
            </div>
        </div>
    </div>
</x-layout>
