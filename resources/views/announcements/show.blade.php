<x-layout>

    <p class="d-none" id="titlePage">Dettaglio</p>


   <div class="min-vh-100">
    <h1 class="text-center fs-2 my-5 d-none">Dettaglio dell'annuncio </h1>
    {{-- @dd($announcement) --}}
    <div class="container mt-5">
        @if ($announcement)
        <div class="row">
            <div class="col-md-6 p-0 shadow-box">
                {{-- carosello immagini --}}
                <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                    {{-- immagini --}}
                    @if ($announcement->images)
                    <div class="carousel-inner">
                        @foreach ($announcement->images as $image)
                        <div class="carousel-item @if($loop->first) active @endif">
                            <img class="img-fluid" src="{{$image->getUrl(660,440)}}"/>
                        </div>
                        @endforeach
                    </div>


                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                  </div>
                
                {{-- fine carosello --}}
            </div>

            {{-- dettagli --}}
            <div class="col-md-6 px-5 textInDark">
                <h3 class="card-title">Titolo: {{ $announcement->title }}</h3>
                <p class="card-footer mt-3">{{__('ui.publish')}}
                    {{ $announcement->created_at->format('d/m/Y') }} - {{__('ui.author')}}
                    {{ $announcement->user->name ?? '' }}</p>
                <h4 class="card-text">Descrizione: {{ $announcement->body }}</h4>
                <h4 class="card-text">Prezzo: €{{ $announcement->price }}</h4>
                
                <hr>
                <h4>Categoria: {{ $announcement->category->name }}</h4>
                <hr>
                <h5 class="fw-bold">Tags</h5>
                @foreach ($announcement->images as $image)
                @foreach ($image->labels as $label) 
                    <p class="d-inline">{{$label}},</p>
                @endforeach
                @endforeach

                <a href="{{route('announcements.index')}}" class="btn btn_giallo w-100 mt-5">{{__('ui.back')}}</a>
            </div>
            {{-- dettagli fine --}}

            <div class="row bord bacheca mt-5">
                <div class="col-12 col-md-3">
                    <h4 class="border border-dark radious-0 bg-third py-2 text-center">{{ __('ui.arrivals') }}</h4>
                </div>

                <div class="row ps-2 ms-1">
                    @foreach ($announcements as $announcement)

                    <div class="col-12 col-md-3 postit">
                        <div class="">
                            <img src="{{ !$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(660, 440): 'https://picsum.photos/400' }}"
                            class="card-img-top in_postit" alt="...">

                            <div class="p-0">
                                <h4 class="card-title ">{{ $announcement->title }}</h4>
                                <p class="card-text">{{ $announcement->body }}</p>
                                <h4 class="card-text"> {{ __('ui.price') }}: € {{ $announcement->price }}</h4>
                                <a href="{{ route('announcement.show', compact('announcement')) }}"
                                class="btn btn_verde_postit">{{ __('ui.view') }}</a>

                                {{-- <a href="#" class="btn btn-primary">Categoria:{{$announcement->category->name}}</a> --}}

                                <p class="card-footer mt-2 mb-0 text-end">{{ __('ui.publish') }}
                                    {{ $announcement->created_at->format('d/m/Y') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>



            </div>
        @endif
        </div>
        @endif
    </div>
   </div>
</x-layout>
