<x-layout>
    <div class="min-vh-100">
        <x-slot name='title'>Presto.it | Annunci</x-slot>
    
        <div class="container">
            {{-- Tasti filtri --}}
            <div class="row mt-0 p-0">
                {{--  CERCA --}}
                <div class="col-10 p-0">
                    <nav class="">
                        <form class="container px-0" action="{{route('announcements.search')}}" metod='GET'>
                            <div class="input-group">
                                <span class="input-group-text border-dark rounded-0 bg-maincolor fs-button fs-6" id="basic-addon1">
                                    <i class="bi bi-search text-white"></i>
                                </span>
                                <input name="searched" class='form-control border-dark rounded-0 bg-mainlight' type="search" aria-label='Search' placeholder="Macchina, Vestiti...">
                            </div>
                        </form>
                    </nav>
                </div>
                {{-- FINE CERCA --}}
                
                {{-- FILTRA --}}
                <div class="col-2 m-0 p-0">
                    <a class="btn btn-main w-100 border border-dark rounded-0 btn_giallo" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">{{__('ui.filter1')}}</a>
                    
                    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                        <div class="offcanvas-header z-10">
                            <h5 class="offcanvas-title fw-bold" id="offcanvasExampleLabel">{{__('ui.filter')}}</h5>
                            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body">
                            {{-- ORDINA --}}
                            <div class="col-12 m-0 p-0">
                                <div class="dropdown btn_verde mt-10">
                                    <button class="btn btn-main dropdown-toggle w-100 border-dark rounded-0" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        {{__('ui.ordin')}}
                                    </button>
                                    <ul class="dropdown-menu w-100 rounded-0 bg-mainlight" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="{{route('priceDesc')}}">{{__('ui.decreasing')}}</a></li>
                                        <li><a class="dropdown-item" href="{{route('priceAsc')}}">{{__('ui.increasing')}}</a></li>
                                        <li><a class="dropdown-item" href="{{route('showByFirst')}}">{{__('ui.last')}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            {{-- FINE ORDINA --}} 
                        </div>
                    </div>
                </div>
                {{-- FINE FILTRA --}}
                
                
            </div>
            
            {{-- Titolo --}}
            <div class="row">
                <div class="col-12 m-0 p-0">
                    <h1 class="my-3 text-center textInDark" id='titlePage'>{{__('ui.our')}}</h1>
                </div>
            </div>
        </div>
        
        
        
        
        {{-- Index annunci --}}
        <div class="container">
            <div class="row">
                @forelse ($announcements as $announcement)
                <div class="col-12 col-md-3 my-3">
                    <div class="card">
                        <img src="{{!$announcement->images()->get()->isEmpty() ? $announcement->images()->first()->getUrl(660,440) :'https://picsum.photos/200'}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title">{{$announcement->title}}</h4>
                            <h4 class="card-text">{{$announcement->body}}</h4>
                            <h5 class="card-text fw-bold">{{__('ui.price')}}: €{{$announcement->price}}</h5>
                            
                            <a href="{{route('announcement.show', compact('announcement'))}}" class="btn btn_verde">{{__('ui.view')}}</a>
                            {{-- <a href="#" class="btn btn-primary">Categoria: {{$announcement->category->name}}</a> --}}
                            <a href="#" class="btn_giallo btn rounded-0 border border-dark"> {{ $announcement->category->name }}</a>
                            
                            <p class="card-footer mt-3">{{__('ui.publish')}} {{$announcement->created_at->format('d/m/Y')}}</p>
                        </div>
                    </div>
                </div>
                @empty
                <div class='col-12'>
                    <div class='alert alert-warning py-3 shadow'>
                        <p class='lead'>{{__('ui.change')}}</p>
                    </div>
                </div>
                @endforelse
                {{$announcements->links()}}
            </div>
        </div>
    </div>
</x-layout>

{{-- {{__('ui.category')}} --}}