<header class="d-none d-sm-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4 p-0">
                {{-- Live Search Livewire --}}
                <div class="container sticky">
                    <div class="row">
                        <div class="col-12 px-0">
                            {{-- <div class="input-group"> --}}

                            @livewire('ads-search-bar')

                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
                {{-- Fine Live Search Livewire --}}
            </div>
            <div class="col-md-4 p-8">
                <img src="/frontend/logoprestosofisticato.png" alt="" class="img-fluid">
            </div>
            <div class="col-md-2 text-end">
                <i class="bi bi-heart btn_rosso fs-3 px-1 pt-1"></i>
                <i class="bi bi-cart3 fs-3 btn_giallo px-1 pt-1"></i>
                {{-- carrello --}}
            </div>
            <div class="col-md-2">
                @guest
                    
                @else
                {{-- Button user profile --}}
                <li class="nav-link nav-item dropdown pb-1">

                    <a class="nav-link active profile text-end py-2 pe-2 dropdown-toggle textInDark" href="#" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="bi bi-person"></i> {{ Auth::user()->name }}
                    </a>


                    <ul class="dropdown-menu rounded-0 w-100">
                       
                        <li>
                            <a class="dropdown-item pe-5" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{__('ui.out')}}</a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
                {{-- fine button profile --}}
                @endguest
                {{-- carrello --}}
            </div>
        </div>
    </div>
    <x-navbar/>



    
</header>