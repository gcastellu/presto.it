<nav class="navbar navbar-expand-lg d-none d-sm-block p-0">
    <div class="container border border-dark p-2 bg-maincolor border ">
        <a class="navbar-brand" href="#"></a>

        {{-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button> --}}

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-lg-0">
                {{-- homepage --}}
                <li class="nav-item">
                    <a class="nav-link p-2 me-3 home" aria-current="page" id="homebutton" href="{{ route('welcome') }}">{{__('ui.Home')}}</a>
                </li>

                {{-- Announcements --}}
                <li class="nav-link  p-0">
                    <a class="nav-link   me-3 p-2 ads" aria-current="page" id="adsButton"
                        href="{{ route('announcements.index') }}">{{__('ui.ads')}}</a>
                </li>
            
                @guest
                    <li class="nav-item">
                        <a id="accessButton" class="nav-link me-3 p-2 access" href="{{ route('login') }}">{{__('ui.access')}}</a>
                    </li>

                @else
                    <li class="nav-item">
                        <a class="nav-link   me-3 p-2 insert" href="{{ route('announcements.create') }}">{{__('ui.ad_ads')}}</a>
                    </li>

                    {{-- Button revisor --}}
                    @if (Auth::User()->is_revisor)
                        <li class="nav-item">
                            <a class="nav-link  position-relative me-2 pe-4 revisor"
                                href="{{ route('revisor.index') }}">{{__('ui.admin')}}
                                <span class="position-absolute top-0 px-1 end-0 bg-danger rounded-circle text-white">
                                    {{ App\Models\Announcement::toBeRevisionedCount() }}
                                    <span class="visually-hidden">unread messages</span>
                                </span>
                            </a>
                        </li>
                    @endif

                    
                @endguest
            </ul>
        </div>
    </div>

    {{-- search sotto la navbar --}}

</nav>
