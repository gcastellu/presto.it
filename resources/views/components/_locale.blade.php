<form action="{{ route('set_language_locale', $lang) }}" method="POST">
    @csrf
    <button type='submit' class='btn p-0 ms-md-3 mx-md-1 my-0'>
        @switch($nation)
            @case('it')
                <img class='img-fluid border border-light' src="/frontend/ita.png" alt="" style="height: 15px;">
            @break

            @case('en')
                <img class='img-fluid border border-light' src="/frontend/eng.png" alt="" style="height: 15px;">
            @break

            @case('de')
                <img class='img-fluid border border-light' src="/frontend/te.png" alt="" style="height: 15px;">
            @break

            @default
                {{-- <img class='img-fluid' src="/public/frontend/ita.png" alt="" style="height: 20px"> --}}
        @endswitch
    </button>
</form>