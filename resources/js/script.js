// carosello categorie
$(function(){
  $('.slider').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 4,
    arrows: false,
    prevArrow:'<i class="bi bi-chevron-left d-flex justify-content-center align-items-center left_arrows slick-arrow"></i>',
    nextArrow:'<i class="bi bi-chevron-right d-flex justify-content-center align-items-center right_arrows"></i>',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 3,
          infinite: true,
          dots: false,
          arrows: true,
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          arrows: false
        }
      },
      {
        breakpoint: 300,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: false,
        }
      }
    ]
  });
});

//? carosello team
$(function(){
  $('.slider_team').slick({
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    prevArrow:'<i class="bi bi-chevron-left d-flex justify-content-center align-items-center left_arrows slick-arrow"></i>',
    nextArrow:'<i class="bi bi-chevron-right d-flex justify-content-center align-items-center right_arrows"></i>',
    responsive: [
      {
        breakpoint: 2000,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 3,
          infinite: false,
          dots: false,
          arrows: true,
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 390,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
        }
      }
    ]
  });
});



const subtitleMob = document.querySelector('#subtitleMob');
let titlePage = document.querySelector('#titlePage');
const searchZoneInHeadMob = document.querySelector('#searchZoneInHeadMob');
const homeButtonMob = document.querySelector('#homeButtonMob');
const adsButtonMob = document.querySelector('#adsButtonMob');
const accountButtonMob = document.querySelector('#accountButtonMob');
const profileButtonMob = document.querySelector('#profileButtonMob');
const headerFixed = document.querySelector('#headerFixed');
const homebutton = document.querySelector('#homebutton');
const adsButton = document.querySelector('#adsButton');
const accessButton = document.querySelector('#accessButton');


// header mobile
document.addEventListener('scroll' , () => {
  let scrolled = window.scrollY;
    // console.log(scrolled);
    if(scrolled > 300) {
      subtitleMob.classList.add('fs-10');
    } else {
      subtitleMob.classList.remove('fs-10');
    }

    if(scrolled > 400){
      headerFixed.classList.remove('d-none');
      headerFixed.classList.add('headFixAnimEntrate');
      headerFixed.classList.remove('headFixAnimExit');
      headerFixed.classList.remove('mt-5');

    } else {
      headerFixed.classList.remove('headFixAnimEntrate');
      headerFixed.classList.add('headFixAnimExit');
      headerFixed.classList.add('z-8');
      // headerFixed.classList.add('mt-5');
      // headerFixed.classList.add('d-none');
    }
  }
);


switch(true) {
  case (titlePage.innerHTML === 'I nostri annunci'):
    searchZoneInHeadMob.classList.add('d-none');
    adsButtonMob.classList.add('pseudoStatusBar');
    adsButton.classList.add('current');
    break;
  case (titlePage.innerHTML === 'revisore'):
    adsButtonMob.classList.add('pseudoStatusBar')
    break;
  case (titlePage.innerHTML === 'Homepage'):
    homeButtonMob.classList.add('pseudoStatusBar');
    homebutton.classList.add('current');
    break;
  case (titlePage.innerHTML === 'Crea il tuo annuncio' || titlePage.innerHTML === 'Create your own ad' || titlePage.innerHTML === 'Erstellen Sie Ihre eigene Anzeige'):
    addButtonMob.classList.add('pseudoStatusBar');
    break;
  case (titlePage.innerHTML === 'Login' || titlePage.innerHTML === 'Register'):
    accountButtonMob.classList.add('pseudoStatusBar');
    accessButton.classList.add('current');
    break;
  default:
    // code block
}

const checkbox = document.querySelector('#checkbox');

checkbox.addEventListener('click', function() {
  setDarkMode();
})



// Dark Mode 
if(localStorage.getItem('theme') == 'dark') {
  setDarkMode();

  // if(document.querySelector('#checkbox').checked) {
  //   localStorage.setItem('checkbox', true);
  // }
}

function setDarkMode() {
  let isDark = document.body.classList.toggle('darkmode');

  if (isDark) {
    setDarkMode.cheched = true;
    localStorage.setItem('theme', 'dark');
    
    document.querySelector('#checkbox').setAttribute('checked', 'checked');
  
  } else { 
    setDarkMode.cheched = false;
    localStorage.removeItem('theme', 'dark');
  }
}