<?php
return [
    // welcome.blade.php
    'welcome1'=>'Welcome to',
    'welcome2'=>'a platform to buy and sell online',

    'slogan1'=>'Follow your intuition, do as we go nose...',
    'slogan2'=>'what are you waiting for starts now!',

    'insurance1'=>'Check out our insurance',
    'insurance2'=>'assistance 24 hours a day!',
    'insurance3'=>'a secure site that reimburses you in case of scam',

    'placeholderSearch'=>'Search for your ads here',

    'categoryDesk'=>'Browse through ads by category',

    'arrivals'=>'new arrivals',

    'price'=>'Price',

    'view'=>'View',

    'publish'=>'Published on:',

    'work1'=>'Work with ',
    'work2'=>'US',
    'work3'=>'We are looking for the',
    'work4'=>'post auditor',
    'work5'=>'in our team, do you want to work with us?',
    'work6'=>'Become an auditor',

    'founder1'=>'Founder',
    'founder2'=>'I am the founder of early, love to work with my colleagues and spend time with them in and out of the office',
    'founder3'=> 'Art Director',
    'founder4'=> 'I always have too many ideas for new group projects I hope to realize them all but the time is never enough',
    'founder5'=> 'CIO',
    'founder6'=> 'Sport is fundamental for me, it’s a bit like life an endless series of fans where the fundamental elements are to have fun and win.',
    'founder7'=> 'CMO',
    'founder8'=> 'The best strategy is always to show off a big smile, it is the easiest and most effective way an efficient communication',
    'manager1'=>'CTO',
    'manager2'=>'Call me Giramondo, I do smart programming for Presto , at the end of a journey I always go back where my heart takes me Rome',

    // navbar
    'Home'=>'Home',
    'ads'=>'Announcements',
    'access'=>'Login',
    'ad_ads'=>'Insert Ads',
    'admin'=>'Review area',
    'out'=>'Logout',

    // navbar.mobile

    'Home_mob'=>'HOME',
    'ads_mob'=>'ADS',
    'search_mob'=>'SEARCH',
    'account_mob'=>'ACCOUNT',
    'add_mob'=>'ADD',
    'profile_mob'=>'PROFILE',
    'out_mob'=>'LOGOUT',

    // header.mobile

    'payoff'=>'You got a nose for business',

    'no_result'=>'No result',

    'create1'=>'Create your own ad',
    'create2'=>'Announcement title',
    'create3'=>'Description:',
    'create4'=>'Price:',
    'create5'=>'Option ',
    'create6'=>'Select Category',
    'create7'=>'Product image:',
    'create8'=>'Images Preview:',
    'create9'=>'Delete',
    'create10'=>'Create',

    'become1'=>'A user has asked to work with us',
    'become2'=>'Here is his data',
    'become3'=>'Name',
    'become4'=>':Email',
    'become5'=>'If you want to make it an auditor, click here',
    'become6'=>'Revise',

    'user'=>'User:',
    'accept'=>'Accept',
    'decline'=>'Decline',

    'explore'=>'Explore the category',
    'no_ads'=>'There are no ads for this category',
    'no_ads2'=>'Published on ',
    'no_ads1'=>'new ad',
    'author'=>'Author:',

    'filter'=> 'Filter search',
    'filter1'=> 'Filter',
    'ordin'=>'Order',
    'decreasing'=> 'Price descending',
    'increasing' => 'Price increasing',
    'last'=> 'Last entries',
    'our'=> 'Our ads',
    'change'=> 'There are no ads for this search. Try to change',

    'ad'=>'Announcement',
    'back'=> 'Go back',
    'category'=> 'Category',

    'mail'=>'Email Address',
    'password'=>'Password',
    'no_account'=>'If you don’t have an account',
    'no_account1'=>'Registered',
    'no_account2'=>'from here',

    'username'=>'Username',
    'repeat'=> 'Repeat password',

    


    



    
];
