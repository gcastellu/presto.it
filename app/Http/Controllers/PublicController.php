<?php

namespace App\Http\Controllers;

use session;
use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome() {
        $announcements = Announcement::where('is_accepted', true)->take(4)->orderBy('id', 'DESC')->get();
        return view('welcome', compact('announcements'));
    }

    public function categoryShow(Category $category){
        return view('announcements.categoryShow', compact('category'));
    }

    public function searchAnnouncements(Request $request){
        $announcements = Announcement::search($request->searched)->where('is_accepted', true)->paginate(10);
        return view('announcements.index',compact('announcements'));
    }

    public function setLanguage($lang) {
        session()->put('locale', $lang);
        return redirect()->back();
    }

    

    
}




