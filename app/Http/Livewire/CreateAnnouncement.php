<?php

namespace App\Http\Livewire;

use App\Jobs\GoogleVisionLabelImage;
use App\Jobs\GoogleVisionSafeSearch;
use App\Jobs\RemoveFaces;
use Livewire\Component;
use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;


class CreateAnnouncement extends Component
{
    use WithFileUploads;

    public $title;
    public $body;
    public $price;
    public $category;
    public $images = [];
    public $temporary_images;
    public $announcement;



    // Regole per la validazione
    protected $rules = [
        'title' =>'required|min:4',
        'body' =>'required|max:150',
        'price'=>'required|numeric',
        'category'=>'required',
        'temporary_images.*'=>'image|max:2048',
        'images.*'=>'image|max:2048',
    ];

    // Messaggi in caso di errori
    protected $messages = [
        'required'=>'Il campo :attribute è obbligatorio',
        'min'=>'Il campo :attribute è troppo corto',
        'max'=>'Il campo :attribute è troppo lungo',
        'temporary_images.required' => 'L\'immagine è richiesta',
        'temporary_images.*.image' => 'I file inseriti non sono immagini',
        'temporary_images.*.max' => 'Inserire immagini di massimo 2MB',
        'images.image' => 'I file inseriti non sono immagini',
        'images.max' => 'Inserire immagini di massimo 2MB',
    ];

    // Funzione che valida le foto
    public function updatedTemporaryImages(){
        if($this->validate([
            'temporary_images.*'=>'image|max:2048',
        ])) {
            foreach($this->temporary_images as $image) {
                $this->images[] = $image;
            }
        }
    }

    // Rimuove le immagini
    public function removeImage($key){
        if(in_array($key, array_keys($this->images))) {
            unset($this->images[$key]);
        }
    }

    public function store()
    {
        $this->validate();

        // $category = Category::find($this->category);
        // $announcement = $category->announcements()->create([
        //     'title'=>$this->title,
        //     'body'=>$this->body,
        //     'price'=>$this->price,
        // ]);

        $this->announcement = Category::find($this->category)->announcements()->create($this->validate());

        //se sono presenti immagini
        if(count($this->images)){
            foreach($this->images as $image){
                // $this->announcement->images()->create(['path'=>$image->store('images','public')]);
                $newFileName = "announcements/{$this->announcement->id}";
                $newImage = $this->announcement->images()->create(['path'=>$image->store($newFileName,'public')]);
                
                RemoveFaces::withChain([
                    dispatch(new ResizeImage($newImage->path, 660, 440)),
                    dispatch(new GoogleVisionSafeSearch($newImage->id)),
                    dispatch(new GoogleVisionLabelImage($newImage->id))

                ])->dispatch($newImage->id);

            }
                File::deleteDirectory(storage_path('/app/livewire-tmp'));
        }

        $this->announcement->user()->associate(Auth::user());
        $this->announcement->save();

        session()->flash('status', '🎉🎉🎉 Hai inserito il tuo annuncio con successo, attendi che un revisore lo approvi 🎊🎊🎊');
        $this->resetForm();

        // Auth::user()->announcements()->save($announcement);

    }

    public function updated($propertyName) {
        $this->validateOnly($propertyName);
    }

    // Funzione per resettare i campi del form
    public function resetForm() {
        $this->title = '';
        $this->body = '';
        $this->price = '';
        $this->category = '';
        $this->images = [];
        $this->temporary_images = [];
    }

    public function render() {
        return view('livewire.create-announcement');
    }
}
